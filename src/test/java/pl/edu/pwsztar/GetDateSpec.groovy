package pl.edu.pwsztar

import spock.lang.Specification

class GetDateSpec extends Specification {
    def "should get date of birth from id"() {
        given:
        UserId userId = new UserId("60100415355")
        UserId userId1 = new UserId("59081573847")
        UserId userId2 = new UserId("9509239962")
        UserId userId3 = new UserId(" 950923996")
        UserId userId4 = new UserId("950923996 ")
        UserId userId5 = new UserId("asdasvasasda")
        UserId userId6 = new UserId("")
        UserId userId7 = new UserId("04212996986")
        UserId userId8 = new UserId("00283102842")
        when:
        def result = [userId.getDate() == Optional.of('04-10-1960'),
                      userId1.getDate() == Optional.of('15-08-1959'),
                      userId2.getDate() == Optional.empty(),
                      userId3.getDate() == Optional.empty(),
                      userId4.getDate() == Optional.empty(),
                      userId5.getDate() == Optional.empty(),
                      userId6.getDate() == Optional.empty(),
                      userId7.getDate() == Optional.of('29-01-2004'),
                      userId8.getDate() == Optional.of('31-08-2000')]
        then:
        result == [true, true, true, true, true, true, true, true, true]
    }
}

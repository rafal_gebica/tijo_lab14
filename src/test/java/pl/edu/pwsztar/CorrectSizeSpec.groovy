package pl.edu.pwsztar

import spock.lang.Specification

class CorrectSizeSpec extends Specification {

    def "should check correct size"() {
        given:
        UserId userId = new UserId("60100415355")
        UserId userId1 = new UserId("59081573837")
        UserId userId2 = new UserId("9509239962")
        UserId userId3 = new UserId(" 950923996")
        UserId userId4 = new UserId("950923996 ")
        UserId userId5 = new UserId("asdasvasasda")
        UserId userId6 = new UserId("")
        when:
        def result = [userId.isCorrectSize(),
                      userId1.isCorrectSize(),
                      userId2.isCorrectSize(),
                      userId3.isCorrectSize(),
                      userId4.isCorrectSize(),
                      userId5.isCorrectSize(),
                      userId6.isCorrectSize()]
        then:
        result == [true, true, false, false, false, false, false]
    }
}
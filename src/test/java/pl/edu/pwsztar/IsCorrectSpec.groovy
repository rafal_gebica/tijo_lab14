package pl.edu.pwsztar

import spock.lang.Specification

class IsCorrectSpec extends Specification {
    def "should check correct id with algorithm"() {
        given:
        UserId userId = new UserId("60100415355")
        UserId userId1 = new UserId("59081573837")
        UserId userId2 = new UserId("9509239962")
        UserId userId3 = new UserId(" 950923996")
        UserId userId4 = new UserId("950923996 ")
        UserId userId5 = new UserId("asdasvasasda")
        UserId userId6 = new UserId("")
        when:
        def result = [userId.isCorrect(),
                      userId1.isCorrect(),
                      userId2.isCorrect(),
                      userId3.isCorrect(),
                      userId4.isCorrect(),
                      userId5.isCorrect(),
                      userId6.isCorrect()]
        then:
        result == [true, true, false, false, false, false, false]
    }
}

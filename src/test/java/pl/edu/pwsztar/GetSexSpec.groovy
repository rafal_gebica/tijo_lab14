package pl.edu.pwsztar

import spock.lang.Specification

class GetSexSpec extends Specification {

    def "should get sex from id"() {
        given:
        UserId userId = new UserId("60100415355")
        UserId userId1 = new UserId("59081573847")
        UserId userId2 = new UserId("9509239962")
        UserId userId3 = new UserId(" 950923996")
        UserId userId4 = new UserId("950923996 ")
        UserId userId5 = new UserId("asdasvasasda")
        UserId userId6 = new UserId("")
        when:
        def result = [userId.getSex().toString() == "Optional[MAN]",
                      userId1.getSex().toString() == "Optional[WOMAN]",
                      userId2.getSex().toString() == "Optional.empty",
                      userId3.getSex().toString() == "Optional.empty",
                      userId4.getSex().toString() == "Optional.empty",
                      userId5.getSex().toString() == "Optional.empty",
                      userId6.getSex().toString() == "Optional.empty"]
        then:
        result == [true, true, true, true, true, true, true]
    }
}

package pl.edu.pwsztar;

class Main {
    public static void main(String[] args) {
        System.out.println("--- PESEL ---");

        UserId userId1 = new UserId("60100415355");
        UserId userId2 = new UserId("59081573847");
        UserId userId3 = new UserId("04212996986");
        UserId userId4 = new UserId("00283102842");

        userId1.getDate();
        userId2.getDate();
        userId3.getDate();
        userId4.getDate();
    }
}

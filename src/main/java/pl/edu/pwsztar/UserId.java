package pl.edu.pwsztar;

import java.util.Arrays;
import java.util.Optional;

final class UserId implements UserIdChecker {

    private final String id; // NR. PESEL

    public UserId(final String id) {
        this.id = id;
    }

    @Override
    public boolean isCorrectSize() {
        if (isCorrectFormat()) {
            System.out.println(id.length());
            return true;
        } else {
            System.out.println("Wrong Id");
            return false;
        }
    }

    @Override
    public Optional<Sex> getSex() {
        if (isCorrectFormat()) {
            if (id.charAt(9) % 2 == 0) {
                System.out.println("WOMAN");
                return Optional.of(Sex.WOMAN);
            } else {
                System.out.println("MAN");
                return Optional.of(Sex.MAN);
            }
        } else {
            System.out.println("Wrong data");
            return Optional.empty();
        }
    }

    @Override
    public boolean isCorrect() {
        if (isCorrectFormat()) {
            int[] sumControlNumbers = {9, 7, 3, 1, 9, 7, 3, 1, 9, 7};
            int[] idArray = getIntArray();
            int sum = 0;

            for (int i = 0; i < sumControlNumbers.length; i++) {
                sum += idArray[i] * sumControlNumbers[i];
            }
            if (sum % 10 == idArray[10]) {
                System.out.println("Correct");
                return true;
            } else {
                System.out.println("Incorrect");
                return false;
            }
        }
        return false;
    }

    @Override
    public Optional<String> getDate() {
        if (isCorrectFormat()) {
            System.out.println(getFullDate());
            return Optional.of(getFullDate());
        }
        return Optional.empty();
    }

    public boolean isCorrectFormat() {
        return id.matches("\\d+") && id.length() == 11;
    }

    private int[] getIntArray() {
        String[] stringsArray = id.split("");

        return Arrays.stream(stringsArray)
                .mapToInt(Integer::parseInt)
                .toArray();
    }

    private String getYear() {
        int[] idArray = getIntArray();
        int year = (idArray[0] * 10) + idArray[1];
        int month = (idArray[2] * 10) + idArray[3];

        if (month > 80 && month < 93) {
            year += 1800;
        } else if (month > 0 && month < 13) {
            year += 1900;
        } else if (month > 20 && month < 33) {
            year += 2000;
        } else if (month > 40 && month < 53) {
            year += 2100;
        } else if (month > 60 && month < 73) {
            year += 2200;
        }
        return String.valueOf(year);
    }

    private String getMonth(int month) {
        if (month > 80 && month < 93) {
            month -= 80;
        } else if (month > 20 && month < 33) {
            month -= 20;
        } else if (month > 40 && month < 53) {
            month -= 40;
        } else if (month > 60 && month < 73) {
            month -= 60;
        }
        if (month < 10) {
            return '0' + String.valueOf(month);
        }
        return String.valueOf(month);
    }

    private String getDay(int day) {
        if (day < 10) {
            return '0' + String.valueOf(day);
        }
        return String.valueOf(day);
    }

    private String getFullDate() {
        int[] idArray = getIntArray();
        int month = (idArray[2] * 10) + idArray[3];

        return getDay((idArray[4] * 10) + idArray[5]) + "-" + getMonth(month) + "-" + getYear();
    }
}

